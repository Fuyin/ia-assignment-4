package template;

//the list of imports
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Collections;

import logist.LogistSettings;
import logist.Measures;
import logist.behavior.AuctionBehavior;
import logist.behavior.CentralizedBehavior;
import logist.agent.Agent;
import logist.config.Parsers;
import logist.simulation.Vehicle;
import logist.plan.Plan;
import logist.task.Task;
import logist.task.TaskDistribution;
import logist.task.TaskSet;
import logist.topology.Topology;
import logist.topology.Topology.City;

/**
 * A very simple auction agent that assigns all tasks to its first vehicle and
 * handles them sequentially.
 *
 */
@SuppressWarnings("unused")
public class CentralizedTemplate implements CentralizedBehavior {

    private Topology topology;
    private TaskDistribution distribution;
    private Agent agent;
    private long timeout_setup;
    private long timeout_plan;
    private List<CopTask> allCopTasks = new ArrayList<CopTask>();//transform all the tasks to Coptasks
    private int MaxIterNum = 100;
    
    
    @Override
    public void setup(Topology topology, TaskDistribution distribution,
            Agent agent) {
        
        // this code is used to get the timeouts
        LogistSettings ls = null;
        try {
            ls = Parsers.parseSettings("config/settings_default.xml");
        }
        catch (Exception exc) {
            System.out.println("There was a problem loading the configuration file.");
        }
        
        // the setup method cannot last more than timeout_setup milliseconds
        timeout_setup = ls.get(LogistSettings.TimeoutKey.SETUP);
        // the plan method cannot execute more than timeout_plan milliseconds
        timeout_plan = ls.get(LogistSettings.TimeoutKey.PLAN);
        
        this.topology = topology;
        this.distribution = distribution;
        this.agent = agent;
       
    }

    @Override
    public List<Plan> plan(List<Vehicle> vehicles, TaskSet tasks) {
        long time_start = System.currentTimeMillis();
//      CopTaskSet(tasks, vehicles);
//		System.out.println("Agent " + agent.id() + " has tasks " + tasks);
//        Plan planVehicle1 = naivePlan(vehicles.get(0), tasks);
//
//        List<Plan> plans = new ArrayList<Plan>();
//        plans.add(planVehicle1);
//        while (plans.size() < vehicles.size()) {
//            plans.add(Plan.EMPTY);
//        }
        
        List<Plan> slsPlans = SLSPlan(vehicles, tasks);
        
        long time_end = System.currentTimeMillis();
        long duration = time_end - time_start;
        System.out.println("The plan was generated in "+duration+" milliseconds.");

        return slsPlans;
    }

    private Plan naivePlan(Vehicle vehicle, TaskSet tasks) {
        City current = vehicle.getCurrentCity();
        Plan plan = new Plan(current);

        for (Task task : tasks) {
            // move: current city => pickup location
            for (City city : current.pathTo(task.pickupCity)) {
                plan.appendMove(city);
            }

            plan.appendPickup(task);

            // move: pickup location => delivery location
            for (City city : task.path()) {
                plan.appendMove(city);
            }

            plan.appendDelivery(task);

            // set current city
            current = task.deliveryCity;
        }
        return plan;
    }


    
    private List<Plan> SLSPlan(List<Vehicle> vehicles, TaskSet tasks){
    	System.out.println("SLSPlan Start");
    	int vNum = vehicles.size();
    	List<CopTask> InitialAssignment = SelectInitialSolution(vehicles, tasks);
    	System.out.println("complete Initial assignment");
    	List<CopTask> coptasksA = CreateNewEmptyCTList(InitialAssignment);
    	System.out.println("complete CreateNewEmptyCTList");
    	double mincost = 500000;
    	int IterNum = 0;
//    	while(!IsFinalPlan(coptasksA, mincost, IterNum)){
    	while(IterNum < MaxIterNum){
//    		System.out.println("Looping");
    		List<CopTask> coptasksAold = coptasksA;
    		List<List<CopTask>> NeighborA = ChooseNeighbors(coptasksAold, vehicles, tasks);
    		coptasksA = CreateNewEmptyCTList( LocalChoice(NeighborA, vehicles, mincost, coptasksA));
    		IterNum ++;
    	}
    	System.out.println("complete SLS");
    	List<Plan> plans = CopTasksToPlan(coptasksA, vehicles);
    	for(CopTask t:coptasksA) t.printCopTask();
    	return  plans;
    }
   
	/**
	 * Initialization method: initialized the allCopTasks; also need to add nextCopTask to exch copTask;
	 * generate for each vehicle and each task!
	 * @param vehicles, TaskSet tasks
	 * @return List<CopTask>
	 */   
    private List<CopTask> SelectInitialSolution(List<Vehicle> vehicles, TaskSet tasks){//return each vehicle's plans 
        //List<CopTask> All = new ArrayList<CopTask>();
//      1. first initial all the task in to coptask
        List<CopTask> All = initCopTaskwTask(tasks);
        List<Integer> TotalTaskCap_eachV = new ArrayList<Integer>(Collections.nCopies(vehicles.size(), 0));//record the capacity 
        List<Integer> FramTime_eachV = new ArrayList<Integer>(Collections.nCopies(vehicles.size(), 0));
        int v_i=0;
        int p_t=0;
        
        System.out.println("Task num:All size = " + tasks.size() + ":" + All.size());
      //then assign the v
        int i_v = 0;
        for(int i=0;i<All.size();i++){
        	if(i_v >= vehicles.size()) i_v=0;
        	All.get(i).holdV = vehicles.get(i_v);
        	i_v++;
        }
              
//      2. set all pickup time and vehicle, //then assign the picktime and deltim, and deliver right after pick up!!! 
        for(Vehicle v: vehicles){
        	for(int i=0; i<All.size();i++){
        		if(v.id() == All.get(i).holdV.id()){
        			All.get(i).pick_time = FramTime_eachV.get(v.id());
        			All.get(i).del_time = All.get(i).pick_time +1;
//        			All.get(i).SetCarrytime(All.get(i).pick_time, All.get(i).del_time);
        			FramTime_eachV.set(v.id(),(All.get(i).del_time+1));
        		}
        	}
        }
        
        System.out.println("After initial pick and del time: Task num:All size = " + tasks.size() + ":" + All.size());
        
       
//      4. based on the pick up and deliver time, setup the pick_next and del_next coptask. 
//        for(int i=0; i<All.size();i++){
//        	for(int j=0;j<All.size();j++){
//        		if(All.get(i).holdV.id() == All.get(j).holdV.id()){
////        			if((All.get(i).pick_time+1) == All.get(j).pick_time || (All.get(i).pick_time+1) == All.get(j).del_time ){
////        				All.get(i).p_next.add(All.get(j));
////        			}
////        			if((All.get(i).del_time+1) == All.get(j).pick_time || (All.get(i).del_time+1) == All.get(j).del_time){
////        				All.get(i).d_next.add(All.get(j));
////        			}     			
//        		}
//        	}
//        }
        	        
        System.out.println("Final Task num:All size = " + tasks.size() + ":" + All.size());
        for(CopTask t:All) t.printCopTask();
        
        return All;
    }
    
    private List<CopTask> initCopTaskwTask(TaskSet allTask){
    	List<CopTask> initialtask = new ArrayList<CopTask>();
    	for(Task t: allTask){
    		//CopTask constructor: int pickt, int delt, Task ct, Vehicle v
    		initialtask.add(new CopTask(t));
    	}
    	return initialtask;
    }
    
    /**
     * whether all the v's capacity is full;
     * @return true: all v's capacity is full; false; there are vs whose capacity isn't full
     */
    private Boolean allVfull(List<Integer> allVcap, List<Vehicle> v){
        for(int i=0;i <= allVcap.size(); i++){
            if(allVcap.get(i) < v.get(i).capacity()){
                return false;
            }
        }   
        return true;
    }
    
    private List<CopTask> CreateNewEmptyCTList(List<CopTask> A){
    	List<CopTask> Al = new ArrayList<CopTask>(A.size());
    	for(CopTask ct: A){
    		Al.add(new CopTask(ct));
    	}
    	return Al;
    }
    
    private List<List<CopTask>> ChooseNeighbors(List<CopTask> A, List<Vehicle> vehicles, TaskSet tasks){
    	//return multiple possibilities of each vehicle's plans
    	List<List<CopTask>> N = new ArrayList<List<CopTask>>();
    	List<CopTask> Al = CreateNewEmptyCTList(A);
    	
    	//================= Try every change of vehicles===========
    	int v_num = vehicles.size();
    	for(CopTask coptask: Al){
    		for(Vehicle v : vehicles){
    			if(!coptask.holdV.equals(v)){    				
					List<CopTask> Anew = CreateNewEmptyCTList(Al);
					Anew = ChangeVehicle(Anew, coptask, v);
					if(constraint_for_plan(Anew, vehicles)== 0){
//    					for(CopTask t:Anew) t.printCopTask();
//						System.out.println("Changing vehicle");
    					N.add( Anew );
    				}
    			}
    		}
    	}
    	//================= Try every change of order ================
    	for(Vehicle v : vehicles){
    		int vMaxTime = 0;
    		for(CopTask coptask: Al){
    			if(coptask.holdV.equals(v)){
    				if(coptask.del_time> vMaxTime){
    					vMaxTime = coptask.del_time;
    				}
    			}
    		}
    		for(CopTask coptask: Al){
    			for(int tp = coptask.pick_time; tp< vMaxTime; tp++){//new p time
    				for(int td = tp; td<vMaxTime; td++){
		    			List<CopTask> newA = CreateNewEmptyCTList(Al);
		    			newA = ChangeOrder(newA, coptask, tp, td);
		    			if(constraint_for_plan(newA, vehicles)==0){
//		    				System.out.println("Changing order");
//		    				for(CopTask t:newA) t.printCopTask();
		    				N.add(newA);
						}
    				}
	    		}
    		}
//    		
    	}
    	System.out.println("length of N = "+N.size());
    	return N;
    }
    
    private List<CopTask> ChangeOrder(List<CopTask> A, CopTask coptask, int newPtime, int newDtime){
		List<CopTask> Al = CreateNewEmptyCTList( A );
		CopTask ct = Al.get(Al.indexOf(coptask));
//		1. remove from the original vehicle's task list
		RemoveFromList(ct,Al);
		
//		2. add to the current vehicle's next CopTask
		AddToList(ct,Al, ct.holdV, newPtime, newDtime);

		return Al;
    }

    
    private void TimeShift(List<CopTask> Al, CopTask ct, Vehicle v, int s){
    	for(CopTask ctinA : Al){
    		if(ctinA.holdV.equals(v)){
				if(ctinA.pick_time >=ct.pick_time){
					ctinA.pick_time += s;
				}
				if(ctinA.del_time >=ct.pick_time){
					ctinA.del_time += s;
				}
    		}
		}
    }
    
    private void RemoveFromList(CopTask ct,List<CopTask> Al){
//    	shift the time index
		TimeShift(Al, ct, ct.holdV, -1);
    }
    
    
    private void AddToList(CopTask ct, List<CopTask> Al, Vehicle v, int newPtime, int newDtime){
//    	randomly assign the order 
    	int maxtime = -1;
    	for(CopTask c: Al){
    		if(c.holdV.equals(v)){
    			if(c.del_time > maxtime){
    				maxtime = c.del_time;
    			}
    		}
    	}
    	if(newPtime == 0 && newDtime ==0){
    		Random rand = new Random();
    		ct.pick_time = rand.nextInt(maxtime+1);
    		ct.del_time = ct.pick_time+1 + rand.nextInt(maxtime+1 - ct.pick_time);
    	}
    	else{
    		ct.pick_time = newPtime;
    		ct.del_time = newDtime;
    	}
    	

    	TimeShift(Al, ct, ct.holdV, 1);
//		
    }
    
    private List<CopTask> ChangeVehicle(List<CopTask> A, CopTask coptask,Vehicle v){
		List<CopTask> Al = CreateNewEmptyCTList( A );
		CopTask ct = Al.get(Al.indexOf(coptask));
//		1. remove from the original vehicle's task list
		RemoveFromList(ct,Al);
		
//		2. add to the current vehicle's next CopTask
		ct.holdV = v;
		AddToList(ct,Al, v, 0, 0);//ptime and dtime both == 0 means randomly assign the order.

		return Al;
    }
    private List<CopTask> LocalChoice(List<List<CopTask>> N, List<Vehicle> vehicles, double mincost,List<CopTask> coptasksA){//return each vehicle's plans
    	List<CopTask> Aselect = new ArrayList<CopTask>();
    	double cost = 0;
    	for(List<CopTask> n : N){
    		List<CopTask> A = CreateNewEmptyCTList(n);
    		if(cost < costFunction(A, vehicles)){
    			cost = costFunction(A, vehicles);
    			Aselect = A;
    		}
    	}
//    	if(cost < mincost){
    	for(CopTask t:Aselect) t.printCopTask();
    	return Aselect;
//    	}
//    	else{
//    		return coptasksA;
//    	}
    }
    private double costFunction(List<CopTask> A, List<Vehicle> vehicles){
    	//double cost = 0;
    	double total_cost = 0;
    	
    	double distance=0;
    	for(Vehicle v: vehicles){//for all vehicle
        	Plan p = new Plan(v.getCurrentCity());
        	City currentC = v.getCurrentCity();
        	distance = 0;
            for(int Ftime=0; Ftime < 60; Ftime++){
                for(CopTask currentCopTask : A){
                    if(currentCopTask.holdV.id() == v.id()){                    	
                        if(currentCopTask.pick_time == Ftime){
                            distance += currentC.distanceTo(currentCopTask.current.pickupCity); //dis
                            currentC = currentCopTask.current.pickupCity;
                            p.appendPickup(currentCopTask.current);
                        }
                        if(currentCopTask.del_time == Ftime){
                            distance += currentC.distanceTo(currentCopTask.current.deliveryCity);
                            currentC = currentCopTask.current.deliveryCity;
                        }
                    }
                }
            }
            total_cost += distance*v.costPerKm();
        }
   	       	
    	return total_cost;
    }
    private double dist(CopTask ti, CopTask tj){
    	return topology.cities().get( topology.cities().indexOf(ti.current.deliveryCity) ).distanceTo( tj.current.pickupCity ) ;
    }
    
	/**
	 * convert CopTask List into List<Plan>; List<Plan>[0] is the plan for vehicle[0]
	 * @param List<CopTask>
	 * @param List<Plan>
	 */ 
    private List<Plan> CopTasksToPlan(List<CopTask> finalcopTasks, List<Vehicle> vs){
    	List<Plan> plans = new ArrayList<Plan>();
        int v_size = vs.size();
//        for(int i=0;i<v_size;i++){
//            plans.add(Plan.EMPTY);
//        }
        
        System.out.println("Best solution Cost: " + costFunction(finalcopTasks, vs));
        
        
        int plan_index=0;//record the index of plans
        boolean isFinal = false;
        for(Vehicle v: vs){//for all vehicle
        	System.out.println("------- Vehicle #: " + v.id() + "-------");
        	Plan p = new Plan(v.getCurrentCity());
        	City currentC = v.getCurrentCity();
            for(int Ftime=0; Ftime < 60; Ftime++){
                //isFinal = true;
            	System.out.println("Frame " + Ftime + ":");
                for(CopTask currentCopTask : finalcopTasks){
                    if(currentCopTask.holdV.id() == v.id()){                    	
                        if(currentCopTask.pick_time == Ftime){
                            //plan_index = currentCopTask.holdV.id();
                            //Plan p = new Plan();
                            //move to the task pick up city and pick up
                            System.out.println("Current City: " + currentC.name);
                            for(City c: currentC.pathTo(currentCopTask.current.pickupCity)){
                                p.appendMove(c);
                                System.out.println("Move to " + c.name);
                            }
                            currentC = currentCopTask.current.pickupCity;
                            p.appendPickup(currentCopTask.current);
                            System.out.println("Pick up task " + currentCopTask.current.id);
                           // isFinal = false;
                        }
                        if(currentCopTask.del_time == Ftime){
                            //plan_index = currentCopTask.holdV.id();
                            System.out.println("Current City: " + currentC.name);
                            for(City c: currentC.pathTo(currentCopTask.current.deliveryCity)){
                                p.appendMove(c);
                                System.out.println("Move to " + c.name);
                            }
                            currentC = currentCopTask.current.deliveryCity;
                            p.appendDelivery(currentCopTask.current);
                            System.out.println("Deliver task " + currentCopTask.current.id);
                          //  isFinal = false;
                        }
                    }
                }
            }
          plans.add(p);  
        }
    	return plans;
    }
    
//    ==================Constraints==================
    private int constraint_for_task(CopTask coptask){
    	int err_code = 0;//In the end, if err_code is still equal to 0, means passing all the constraint
//    	constraints related to time
    	if(coptask.del_time <= coptask.pick_time){
    		err_code = 1;
//    		System.out.println("Constraint1: this task's nextTask is equal to itself!");
    	}
    	else if(coptask.holdV==null){
			err_code = 2;
			System.out.println("Constraint2: Not all the tasks are assigned to the vehicles!");
		}
		else if(coptask.del_time == 0){
			err_code = 24;
			System.out.println("Constraint3: Not all the tasks are delivered!");
		}

    	return err_code;
    }
    private int constraint_for_vehicle(Vehicle v, List<CopTask> A){
    	int err_code = 0;
    	int end_time = 0;
    	List<CopTask> tasksCar = new ArrayList<CopTask>();
    	for(CopTask copt: A){
    		if(copt.holdV.equals(v)){
    			tasksCar.add(copt);
    			if(end_time<copt.del_time) end_time = copt.del_time;
    		}
    	}
    	
//    	see if there is any time frame the vehicle is overcarried
    	int weight_sum = 0;
    	for(int t=0;t<end_time;t++){
//    		int tasksAtThisTime = tasksSchedule.get(t).size();
    		for(CopTask ct : tasksCar){
    			if(ct.pick_time == t){
    				weight_sum += ct.current.weight;
    			}
    			else if(ct.del_time == t){
    				weight_sum -= ct.current.weight;
    			}
    		}
    		if(weight_sum>v.capacity()){
    			err_code = 11;
    			System.out.println("Constraint11: The vehicle is overcarried!");
    			break;
    		}
    	}

    	return err_code;
    }
    
    private int constraint_for_plan(List<CopTask> A, List<Vehicle> vehicles){
    	int err_code = 0;
//    	see if all the tasks are all assigned
    	for(CopTask ct: A){
    		err_code += constraint_for_task(ct);
    	}
    	for(Vehicle v: vehicles){
    		err_code += constraint_for_vehicle(v, A);
    	}
    	return err_code;
    }
    

}
