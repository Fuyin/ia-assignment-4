package template;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import logist.LogistSettings;

import logist.agent.Agent;
import logist.config.Parsers;
import logist.simulation.Vehicle;
import logist.plan.Plan;
import logist.task.Task;
import logist.task.TaskDistribution;
import logist.task.TaskSet;
import logist.topology.Topology;
import logist.topology.Topology.City;
import logist.Measures;
import logist.behavior.AuctionBehavior;


public class CopTask {
	public int pick_time=0; //pick time of current task
	public int del_time=0; //deliver time of current task
//	public List<Integer> carry_time = new ArrayList<Integer>(); //carry time
//	public List<CopTask> p_next = new ArrayList<CopTask>();//next task after picking up this task
//	public List<CopTask> d_next = new ArrayList<CopTask>();//next task after delivering this task
	public Task current;//the current task
	public Vehicle holdV; //the vehicle that hold this task
	
	
	public CopTask(int pickt, int delt, Task ct, Vehicle v){
		this.pick_time = pickt;
		this.del_time = delt;
		this.current = ct;
		this.holdV = v;
		
	}
	
	public CopTask(Task t){ //this is for initialSolution and the other parameter will be assigned later
		this.current = t;
		
	}
	
	
	public CopTask(CopTask old) {
		this.pick_time = old.pick_time;
		this.del_time = old.del_time;
		this.current = old.current;
		this.holdV = old.holdV;
	}
	
//	public void SetCarrytime(int pt, int dt){
//		List<Integer> ct = new ArrayList<Integer>();
//		for(int i=1; i< ((dt - pt)); i++){
//			ct.add(pt+i);
//		}
//		this.carry_time = ct;
//	}
//	public void removeNextTaskPick(CopTask nextOne){
//		this.p_next.remove(this.p_next.indexOf(nextOne));
//	}
//	public void removeNextTaskDel(CopTask nextOne){
//		this.d_next.remove(this.d_next.indexOf(nextOne));
//	}
//	
//	public void addnextcopTaskPick(int picktime, int deltime, Task nextTask){//can call this task from outside to add NextCopTask
//		CopTask nextCop = new CopTask(picktime, deltime,nextTask, holdV);
//		p_next.add(nextCop);
//	}
//	
//	public void addnextcopTaskDel(int picktime, int deltime, Task nextTask){//can call this task from outside to add NextCopTask
//		CopTask nextCop = new CopTask(picktime, deltime,nextTask, holdV);
//		d_next.add(nextCop);
//	}
	
	public void printCopTask(){
		 System.out.println("----------------------------------------------");
		 System.out.println("TaskID: " + this.current.id + " pick_time: " + this.pick_time + " del_time: " + this.del_time + " holdV id: " + this.holdV.id() );
		 
//		 System.out.print("P_next tasks ID:");
//		 for(CopTask ptask: p_next){//p_next
//			 System.out.print(ptask.current.id);
//		 }
//		 System.out.println();
//		 
//		 System.out.print("D_next tasks ID:");
//		 for(CopTask dtask: d_next){//d_next
//			 System.out.print(dtask.current.id);
//		 }
//		 System.out.println();
	}
	
	@Override
	public boolean equals(Object obj) {//for comparing if the queue contains the state
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CopTask other = (CopTask) obj;
//		if (Double.doubleToLongBits(pick_time) != Double
//				.doubleToLongBits(other.pick_time))
//			return false;
//		if (Double.doubleToLongBits(del_time) != Double
//				.doubleToLongBits(other.del_time))
//			return false;		
//		if (carry_Ts == null) {
//			if (other.carry_Ts != null)
//				return false;
//		} else if (!carry_Ts.equals(other.carry_Ts))
//			return false;		
//		if (carry_time == null) {
//			if (other.carry_time != null)
//				return false;
//		} else if (!carry_time.equals(other.carry_time))
//			return false;		
//		if (next == null) {
//			if (other.next != null)
//				return false;
//		} else if (!next.equals(other.next))
//			return false;
//		if (holdV == null) {
//			if (other.holdV != null)
//				return false;
//		} else if (!holdV.equals(other.holdV))
//			return false;
		if (current == null) {
			if (other.current != null)
				return false;
		} else if (!current.equals(other.current))
			return false;
		return true;
	}
}
